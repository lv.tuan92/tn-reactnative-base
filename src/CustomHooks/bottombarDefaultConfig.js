import { TransitionPresets } from "@react-navigation/stack";
import React, { useState } from "react";
import { View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { Color } from "../Color";
import { horizontal } from "../Scales";
import BackHeader from "../../components/BackHeader";
import { hasNotch } from "react-native-device-info";

export default function bottombarDefaultConfig(
  { navigation, route },
  headerRight
) {
  const { top } = useSafeAreaInsets();
  const [defaultConfig, setDefaultConfig] = useState({
    ...TransitionPresets.SlideFromRightIOS,
    activeTintColor: Color.base_color,
    style: {
      height: hasNotch ? 75 : 70,
    },
    tabStyle: {
      backgroundColor: Color.white,
      height: hasNotch ? 75 : 70,
    },
    labelStyle: {
      fontSize: 12,
      lineHeight: 14,
      marginBottom: hasNotch ? 15 : 10,
    },
  });

  return defaultConfig;
}
