import {getFocusedRouteNameFromRoute} from '@react-navigation/native';
import {TransitionPresets} from '@react-navigation/stack';
import React, {useState} from 'react';
import {View} from 'react-native';
import {hasNotch} from 'react-native-device-info';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import IconHelp from '../assets/tabbar/icon_help.svg';
import Constants from '../Constant';
import {Color} from '../helper/colors/index.js';
import {isIOS} from '../Platforms';
import {horizontal} from '../Scales';
import TouchableDebounce from '../components/TouchableDebounce/index.js';
import BackHeader from '../components/BackHeader';

export default function useDefaultConfig({navigation, route}, headerRight) {
  const routeName = getFocusedRouteNameFromRoute(route);
  const {top} = useSafeAreaInsets();
  /**
   * Custom handle button headerRight.
   */
  const onHeaderRight = () => {
    navigation.navigate(Constants.PDF_SCREEN);
  };
  const [defaultConfig, setDefaultConfig] = useState({
    ...TransitionPresets.SlideFromRightIOS,
    headerTitleStyle: {
      textAlign: 'center',
    },
    headerTitleStyle: {
      textAlign: 'center',
      textAlignVertical: 'center',
      alignSelf: 'center',
      fontWeight: 'bold',
      fontSize: 16,
      paddingHorizontal: horizontal(5),
    },
    headerStyle: {
      backgroundColor: Color.white,
      height: isIOS && hasNotch() ? 85 : 60,
    },
    headerTintColor: Color.cl_text_title,
    headerLeft: () => <BackHeader {...navigation} />,
    headerRight: () => (
      <TouchableDebounce
        style={{right: horizontal(15)}}
        onPress={onHeaderRight}>
        <IconHelp width={24} height={24} />
      </TouchableDebounce>
    ),
  });

  return defaultConfig;
}
