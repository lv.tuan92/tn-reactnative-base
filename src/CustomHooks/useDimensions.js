import { useState } from "react";
import { screenHeight, screenWidth } from "../Platforms";

function useDimensions() {
  const [dimension, setDimension] = useState({
    scrWidth: screenWidth,
    scrHeight: screenHeight,
  });
  return dimension;
}

export default useDimensions;
