export default {
  regexPassword: /^[a-zA-Z0-9]{8,}$/,
  regexEmail:
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
  regexPhone:
    /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i,
  regexNumber: /^\d+$/,
  regexZip: /^\d{5}$|^\d{5}-\d{4}$/,
  regexRoutingNumber: /^\d{0,9}$/,
  regexAlphabet: /^[A-Za-z ]+$/,
  regexPrice: /^\d+(\.\d{1,2})?$/,
  WEB_CLIENT_ID: '1001275266289-ql7723lnp807ohnagu7g6rdr52n8qs8a.apps.googleusercontent.com',

  /**
   * name
   */
  BOTTOM_TAB: 'BOTTOM_TAB',
  AUTHOR_STACK: 'AUTHOR_STACK',
  USER_STACK: 'USER_STACK',

  /**
   * name stack.
   */
  HOMESTACK: 'HOMESTACK',
  HOME_TITLE_TAB: 'Home',
  HOME_SCREEN: 'Home',

  PROFILE_STACK: 'PROFILE_STACK',
  PROFILE_TITLE_TAB: 'Profile',
  PROFILE_SCREEN: 'Profile',
  PDF_SCREEN: 'Pdf',

  /**
   * name screen.
   */
  INTRODUCTION: 'INTRODUCTION',
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
  SIGNUP: 'SIGNUP',
  HOME: 'Home',
  CHANGE_PASSWORD_SCREEN: 'Change Password',
  HELP_SCREEN: 'HELP_SCREEN',
  POLICY_SCREEN: 'POLICY_SCREEN',

  /**
   * AsyncStorage.
   */
  USER_NAME: 'USER_NAME',
  DATA_USER: 'DATA_USER',
  LANGUAGE_APP: 'LANGUAGE_APP',

  /**
   * Network
   */
  DATA_SUCCESS: [200, 201, '200', '201'],
  LANGUAGE_VN: 'vn',
};
