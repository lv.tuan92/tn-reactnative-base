import React, {useEffect, useCallback, useMemo, useState, useRef} from 'react';
import PropTypes from 'prop-types';
import {
  SafeAreaView,
  Text,
  BackHandler,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import styles from './ChangePassword.styles';
import {isIOS} from '../../Platforms';
import {hasNotch} from 'react-native-device-info';
import CMTextInput from '../../components/CMTextInput';
import Constant from '../../Constant';
import {useSelector} from 'react-redux';

ChangePassword.propTypes = {};

ChangePassword.defaultProps = {};

function ChangePassword() {
  const languageLocal = useSelector(state => state.appReduces.language);
  const [currPassWord, setCurrPassWord] = useState('');
  const [newPassWord, setNewPassWord] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [validCurPass, setValidCurPass] = useState(false);
  const [validNewPass, setValidNewPass] = useState(true);
  const [validConfirmPass, setValidConfirmPass] = useState(true);
  const [errorKeyPass, setErrorKeyPass] = useState();
  const [errorKeyConfirmPass, setErrorKeyConfirmPass] = useState();
  const [errorKeyValidConfirmPass, setErrorKeyValidConfirmPass] = useState();
  const refCurPass = useRef(null);
  const refNewPass = useRef(null);
  const refConfimPass = useRef(null);

  var PLACEHOLDER = {
    en: {
      cur_password: 'Enter current password',
      new_password: 'Enter new password',
      valid_new_password: 'Confirm new password',
    },
    vn: {
      cur_password: 'Nhập mật khẩu hiện tại',
      new_password: 'Nhập mật khẩu mới',
      valid_new_password: 'Xác nhận lại mật khẩu mới',
    },
  };

  const handleChangePass = () => {};
  
  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView
        style={styles.container}
        keyboardVerticalOffset={isIOS && hasNotch() ? 100 : 10}
        behavior={isIOS ? 'padding' : undefined}
        enabled={isIOS}>
        <ScrollView>
          <CMTextInput
            textKeyTitle={'text-curent-password-input'}
            isInputRequied={true}
            isPassWord={true}
            maxLength={100}
            isValid={validCurPass}
            errorKey={'text-incorrect-password'}
            inputRef={refCurPass}
            returnKeyType={'next'}
            onSubmitEditing={() => refNewPass.current.focus()}
            blurOnSubmit={false}
            placeholder={
              languageLocal == Constant.LANGUAGE_VN
                ? PLACEHOLDER.vn.cur_password
                : PLACEHOLDER.en.cur_password
            }
          />
          <CMTextInput
            textKeyTitle={'text-new-password-input'}
            isInputRequied={true}
            isPassWord={true}
            isValid={validNewPass}
            inputRef={refNewPass}
            returnKeyType={'next'}
            onSubmitEditing={() => refConfimPass.current.focus()}
            blurOnSubmit={false}
            placeholder={
              languageLocal == Constant.LANGUAGE_VN
                ? PLACEHOLDER.vn.new_password
                : PLACEHOLDER.en.new_password
            }
          />
          <CMTextInput
            textKeyTitle={'text-comfirm-password-input'}
            isInputRequied={true}
            isPassWord={true}
            maxLength={100}
            isValid={validConfirmPass}
            inputRef={refConfimPass}
            returnKeyType={'next'}
            onSubmitEditing={handleChangePass}
            blurOnSubmit={false}
            placeholder={
              languageLocal == Constant.LANGUAGE_VN
                ? PLACEHOLDER.vn.valid_new_password
                : PLACEHOLDER.en.valid_new_password
            }
          />
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

export default React.memo(ChangePassword);
