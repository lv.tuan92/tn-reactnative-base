import React, {
  useEffect,
  useCallback,
  useMemo,
  useState,
  useContext,
  useLayoutEffect,
} from 'react';
import {SafeAreaView, Text, View} from 'react-native';
import styles from './ProfileScreen.styles';
import Constant from '../../Constant';
import TouchableDebounce from '../../components/TouchableDebounce';
import IconEdit from '../../assets/tabbar/icon_edit.svg';
import {horizontal} from '../../Scales';
import ProfileScreenPlaceholder from './Profile.Placeholder';
import CMText from '../../components/CMText';
import FastImage from 'react-native-fast-image';
import IconPassWord from '../../assets/profile/icon_change_pass.svg';
import IconHelp from '../../assets/profile/icon_help.svg';
import IconLogout from '../../assets/profile/icon_logout.svg';
import IconPolicy from '../../assets/profile/icon_policy.svg';
import ItemProfile from './ItemProfile';
import {useSelector} from 'react-redux';

ProfileScreen.propTypes = {};

ProfileScreen.defaultProps = {};

const listProfile = [
  {
    name: Constant.CHANGE_PASSWORD_SCREEN,
    icon: <IconPassWord width={20} height={20} />,
    keyName: 'text-change-password',
  },
  {
    name: Constant.PDF_SCREEN,
    icon: <IconHelp width={20} height={20} />,
    keyName: 'text-help',
  },
  {
    name: Constant.POLICY_SCREEN,
    icon: <IconPolicy width={20} height={20} />,
    keyName: 'text-policy',
  },
  {
    name: Constant.LOGOUT,
    icon: <IconLogout width={20} height={20} />,
    keyName: 'log-out',
  },
];

function ProfileScreen(props) {
  const {navigation, route} = props;
  const userState = useSelector(state => state.appReduces.userState);
  const [data, setData] = useState(false);

  const onHeaderRight = () => {
    navigation.navigate(Constant.PDF_SCREEN);
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View>
          <TouchableDebounce
            style={{right: horizontal(15)}}
            onPress={onHeaderRight}>
            <IconEdit width={24} height={24} />
          </TouchableDebounce>
        </View>
      ),
    });
  }, []);

  useEffect(() => {
    setTimeout(() => {
      setData(true);
    }, 1500);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      {data ? (
        <>
          <View style={styles.viewHeaderInfor}>
            {userState?.user?.photo ? (
              <FastImage
                source={{uri: userState?.user?.photo}}
                style={styles.imageUser}
                resizeMode={'cover'}
              />
            ) : (
              <FastImage
                source={require('../../assets/profile/man.png')}
                style={styles.imageUser}
                resizeMode={'cover'}
              />
            )}
            <TouchableDebounce>
              <FastImage
                source={require('../../assets/profile/add_image.png')}
                style={styles.imageAdd}
                resizeMode={'cover'}
              />
            </TouchableDebounce>
            <CMText
              title={userState?.user?.name ?? 'Nguyễn Văn A'}
              style={styles.textUser}
            />
            <CMText
              title={`ID: ${userState?.user?.id ?? '#13534434'}`}
              style={styles.textId}
            />
          </View>
          <View>
            {listProfile.map((item, index) => (
              <ItemProfile item={item} navigation={navigation} key={index} />
            ))}
          </View>
        </>
      ) : (
        <ProfileScreenPlaceholder />
      )}
    </SafeAreaView>
  );
}

export default React.memo(ProfileScreen);
