import {StyleSheet} from 'react-native';
import {Color} from '../../helper/colors';
import {horizontal, vertical} from '../../Scales';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: horizontal(15),
  },
  viewHeaderInfor: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLogout: {
    width: '60%',
  },
  imageUser: {
    width: horizontal(100),
    height: horizontal(100),
    borderRadius: horizontal(50),
    marginTop: vertical(30),
  },
  imageAdd: {
    width: horizontal(20),
    height: horizontal(20),
    borderRadius: horizontal(10),
    position: 'absolute',
    bottom: horizontal(5),
    right: horizontal(-40),
    backgroundColor: 'red'
  },
  textUser: {
    marginTop: vertical(5),
  },
  textId: {
    marginVertical: vertical(5),
  },
});
