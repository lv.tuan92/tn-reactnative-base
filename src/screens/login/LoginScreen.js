import React, {useEffect, useCallback, useMemo, useState, useRef} from 'react';
import PropTypes from 'prop-types';
import {
  KeyboardAvoidingView,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import styles from './LoginScreen.styles';
import {useDispatch, useSelector} from 'react-redux';
import {useContext} from 'react';
import {AuthContext} from '../../context';
import TouchableDebounce from '../../components/TouchableDebounce';
import CMText from '../../components/CMText';
import BottomSheetLanguage from '../../components/BottomSheetLanguage';
import FastImage from 'react-native-fast-image';
import {
  updateLanguageAction,
  updateShowDialogWarnAction,
} from '../../redux/actions';
import CommonButton from '../../components/CommonButton';
import {isIOS, screenHeight, screenWidth} from '../../Platforms';
import CMTextInput from '../../components/CMTextInput';
import {Color} from '../../helper/colors';
import {hasNotch} from 'react-native-device-info';
import Constant from '../../Constant';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import RegexPassword from '../../components/RegexPassword';
import ItemSwitchDots from '../../components/ItemSwitchDots';
import FlatlistWrap from '../../components/FlatlistWrap';
import BottomSheetFilter from '../../components/BottomSheetFilter';
import QuestionBottom from '../../components/QuestionBottom';
import BottomSheetQuestion from '../../components/BottomSheetQuestion';

LoginScreen.propTypes = {};

LoginScreen.defaultProps = {};

var PLACEHOLDER = {
  en: {
    userplaceholder: 'Enter username',
    pwplaceholder: 'Enter password',
  },
  vn: {
    userplaceholder: 'Nhập tài khoản',
    pwplaceholder: 'Nhập mật khẩu',
  },
};

function LoginScreen() {
  const dispatch = useDispatch();
  const languageLocal = useSelector(state => state.appReduces.language);
  const {signIn} = useContext(AuthContext);
  const [isOpenModal, setIsOpenModal] = useState(true);

  const [password, setPassword] = useState('');
  const [usename, setUsername] = useState('');
  const [validUser, setValidUser] = useState(true);
  const [validPass, setValidPass] = useState(true);
  const [keyErrorUse, setKeyErrorUse] = useState('');
  const [keyErrorPass, setKeyErrorPass] = useState('');
  const refPassword = useRef(null);
  const refUsename = useRef(null);

  useEffect(() => {
    GoogleSignin.configure({
      webClientId: Constant.WEB_CLIENT_ID,
      offlineAccess: true,
    });
  }, []);

  const navigateUserStack = data => {
    signIn({
      signIn: data.signIn,
      userToken: data.userToken,
      user: data.user,
    });
  };

  /**
   * Check empty for field
   */
  function checkEmptyPassword() {
    return password != '';
  }
  function checkEmptyUsename() {
    return usename != '';
  }

  const login = () => {
    setValidUser(true);
    setValidPass(true);
    setKeyErrorUse('');
    setKeyErrorPass('');
    if (!checkEmptyUsename()) {
      setValidUser(false);
      setKeyErrorUse('text-empty-usename');
    }
    if (!checkEmptyPassword()) {
      setValidPass(false);
      setKeyErrorPass('text-empty-password');
    } else {
      const data = {
        signIn: true,
        userToken: 'dfbdjfjdgfjdbfdjgfgheue5iDSSJH',
        user: {
          photo:
            'https://lh3.googleusercontent.com/a/ACg8ocKUzY_xk1NdDJkmksEh-NbEQC_Hfen-IFsjKcbIXroU=s96-c',
          email: 'sontao5197@gmail.com',
          name: ' Tào Ngọc Sơn',
          id: '102740695402833458670',
        },
      };
      navigateUserStack(data);
    }
  };

  const handleCloseModal = useCallback(() => {
    setIsOpenModal(false);
  }, []);

  /**
   * Handle login google.
   */
  const handleLoginGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices({showPlayServicesUpdateDialog: true});
      const userInfo = await GoogleSignin.signIn();
      const data = {
        signIn: true,
        userToken: userInfo?.idToken,
        user: userInfo?.user,
      };
      navigateUserStack(data);
    } catch (error) {
      signOutGoogle();
    }
  };

  /**
   *
   */
  const signOutGoogle = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
    } catch (error) {
      dispatch(
        updateShowDialogWarnAction({
          isShowModalWarn: true,
          titleHeader: '',
          keyHeader: '',
          keyMessage: '',
          contentMessage: '',
        }),
      );
    }
  };

  return (
    <KeyboardAvoidingView
      style={styles.container}
      keyboardVerticalOffset={isIOS && hasNotch() ? 0 : 10}
      behavior={isIOS ? 'padding' : undefined}
      enabled={isIOS}>
      <SafeAreaView style={styles.container}>
        <FastImage
          source={require('../../assets/login/image_login.png')}
          style={styles.imageBackground}
          resizeMode={'cover'}
        />
        <ScrollView
          style={styles.scrollView}
          showsVerticalScrollIndicator={false}>
          <View style={styles.viewContainer}>
            <View style={styles.viewLanguage}>
              <TouchableDebounce
                onPress={() => {
                  setIsOpenModal(true);
                }}>
                <FastImage
                  resizeMode={'contain'}
                  source={
                    languageLocal === Constant.LANGUAGE_VN
                      ? require('../../assets/language/vn.png')
                      : require('../../assets/language/en.png')
                  }
                  style={styles.iamgeLanguage}
                />
              </TouchableDebounce>
            </View>

            <CMTextInput
              textKeyTitle={'text-username-input'}
              isInputRequied={true}
              isPassWord={true}
              isValid={validUser}
              placeholder={
                languageLocal == Constant.LANGUAGE_VN
                  ? PLACEHOLDER.vn.userplaceholder
                  : PLACEHOLDER.en.userplaceholder
              }
              errorKey={keyErrorUse}
              inputRef={refUsename}
              returnKeyType={'next'}
              onSubmitEditing={() => refPassword.current.focus()}
              blurOnSubmit={false}
              onChangeText={usename => {
                setUsername(usename);
              }}
            />
            <CMTextInput
              textKeyTitle={'text-password-input'}
              isInputRequied={true}
              isPassWord={true}
              isValid={validPass}
              placeholder={
                languageLocal == Constant.LANGUAGE_VN
                  ? PLACEHOLDER.vn.pwplaceholder
                  : PLACEHOLDER.en.pwplaceholder
              }
              errorKey={keyErrorPass}
              inputRef={refPassword}
              returnKeyType={'next'}
              onSubmitEditing={login}
              blurOnSubmit={false}
              onChangeText={password => {
                setPassword(password);
              }}
            />
            {/* <RegexPassword lenghtMax={4} passRegex={3} /> */}
            {/* <ItemSwitchDots textItem={'Bắt buộc'} type={2} status={false} /> */}
            {/* <FlatlistWrap /> */}
            {/* <BottomSheetFilter isOpenModal={isOpenModal} closeModal={handleCloseModal} /> */}
            {/* <QuestionBottom
              countQuestion={20}
              currentQuestion={1}
              onHandleBack={() => {}}
              onHandleNext={() => {}}
            /> */}
            <BottomSheetQuestion isOpenModal={isOpenModal} closeModal={handleCloseModal} />
            <CommonButton
              i18nKey={'login-button'}
              onPress={login}
              style={styles.btnLogin}
            />
            <TouchableDebounce style={styles.btnForgot}>
              <CMText
                i18nKey={'text-forgot-password'}
                style={styles.textForgot}
              />
            </TouchableDebounce>
            <TouchableDebounce
              style={styles.btnLoginGoogle}
              onPress={handleLoginGoogle}>
              <FastImage
                source={require('../../assets/login/icon_google.png')}
                resizeMode={'contain'}
                style={styles.iconGoogle}
              />
              <CMText
                i18nKey={'text-login-with-google'}
                style={styles.textLoginGoogle}
              />
            </TouchableDebounce>
            <View style={styles.viewDonotAccount}>
              <CMText i18nKey={'text-do-not-account'} />
              <TouchableDebounce>
                <CMText i18nKey={'sigup-button'} style={styles.textSignupNow} />
              </TouchableDebounce>
            </View>
          </View>
        </ScrollView>

        {/* <BottomSheetLanguage
          isOpenModal={isOpenModal}
          closeModal={handleCloseModal}
        /> */}
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
}

export default React.memo(LoginScreen);
