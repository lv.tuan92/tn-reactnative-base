import {StyleSheet} from 'react-native';
import {Color} from '../../helper/colors';
import { horizontal, vertical } from '../../Scales';
import { topIndicator } from './Indicartor';

export const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: 'white',
  },
  title: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 25,
    color: Color.cl_text_app,
    lineHeight: 30,
  },
  desc: {
    textAlign: 'center',
    fontSize: 16,
    marginTop: vertical(21),
    color: Color.text_color,
  },
  body: {
    position: "absolute",
    left: 0,
    right: 0,
    paddingHorizontal: horizontal(15),
    top: topIndicator + 30,
    flexDirection: "row",
    justifyContent: "center",
    flexWrap: "wrap",
  },
  button: {
    alignSelf: "center",
    position: "absolute",
  },
});
