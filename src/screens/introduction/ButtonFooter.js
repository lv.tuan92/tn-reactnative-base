import React from 'react';
import {View} from 'react-native';
import NextIcon from '../../assets/introduction/next.svg';
import CommonButton from '../../components/CommonButton';
import RoundButton from '../../components/RoundButton';
import {screenWidth} from '../../Platforms';

ButtonFooter.propTypes = {};

ButtonFooter.defaultProps = {};

export default function ButtonFooter({style, index, onPressNext}) {
  return index === 0 || index === 1 ? (
    <RoundButton icon={<NextIcon />} onPress={onPressNext} style={style} />
  ) : (
    <View style={[{flexDirection: 'row', justifyContent: 'center'}, style]}>
      <CommonButton
        i18nKey={'get-start'}
        onPress={onPressNext}
        style={{width: screenWidth * 0.5}}
      />
    </View>
  );
}
