import React, {useCallback, useRef, useState} from 'react';
import {Text, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Animated, {
  runOnJS,
  useAnimatedScrollHandler,
  useDerivedValue,
  useSharedValue,
} from 'react-native-reanimated';
import Constant from '../../Constant';
import {isIOS, screenWidth} from '../../Platforms';
import {styles} from './Introduction.styles';
import CMText from '../../components/CMText';
import Header from './Header';
import Indicator from './Indicartor';
import ButtonFooter from './ButtonFooter';

const SNAP_INTERVAL = screenWidth;
const imgHeader = require('../../assets/introduction/img_logo.png');

const items = [
  {
    title: 'title 1',
    desc: 'descriptons descriptons descriptons descriptons',
    source: require('../../assets/introduction/img_tutorial_1.png'),
  },
  {
    title: 'title 2',
    desc: 'descriptons descriptons descriptons descriptons',
    source: require('../../assets/introduction/img_tutorial_2.png'),
  },
  {
    title: 'title 3',
    desc: 'descriptons descriptons descriptons descriptons',
    source: require('../../assets/introduction/img_tutorial_3.png'),
  },
];

function Introduction({navigation}) {
  const {top, bottom} = useSafeAreaInsets();
  const [count, setCount] = useState(0);
  const [scroll, setScroll] = useState(true);
  const scrollRef = useRef(null);
  const scrollX = useSharedValue(0);
  const position = useDerivedValue(() => {
    return scrollX.value / SNAP_INTERVAL;
  });

  /**
   * Turn on scroll
   */
  const openScroll = () => {
    setScroll(true);
  };

  const onScrollEvent = useAnimatedScrollHandler({
    onScroll: e => {
      scrollX.value = e.contentOffset.x;
      if (scrollX.value / SNAP_INTERVAL > count + 0.9) {
        runOnJS(setCount)(count + 1);
      } else if (scrollX.value / SNAP_INTERVAL < count - 0.9) {
        runOnJS(setCount)(count - 1);
      }
    },
  });

  const onPressNext = useCallback(() => {
    if (count < items.length - 1) {
      setCount(count + 1);
      scrollRef?.current?.scrollTo({
        x: SNAP_INTERVAL * (count + 1),
        animated: true,
      });
      isIOS ? setScroll(false) : setScroll(true);
    } else {
      navigation.navigate(Constant.LOGIN);
    }
  }, [count]);

  return (
    <View style={[styles.main, {paddingTop: top + 20}]}>
      <Animated.ScrollView
        ref={scrollRef}
        onMomentumScrollEnd={openScroll}
        scrollEnabled={scroll}
        horizontal
        scrollEventThrottle={16}
        bounces={false}
        style={{flexGrow: 1}}
        disableIntervalMomentum
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        snapToInterval={SNAP_INTERVAL}
        onScroll={onScrollEvent}>
        {Array(3)
          .fill(3)
          .map((__, index) => {
            return (
              <View key={index}>
                <Header
                  index={index}
                  imageHeader={imgHeader}
                  imageMain={items[index].source}
                />
                <View style={styles.body}>
                  <Text style={styles.title}>{items[index].title}</Text>
                  <Text style={styles.desc}>{items[index].desc}</Text>
                </View>
                <ButtonFooter
                  style={[
                    styles.button,
                    {
                      bottom: bottom + 20,
                    },
                  ]}
                  index={index}
                  onPressNext={onPressNext}
                />
              </View>
            );
          })}
      </Animated.ScrollView>
      <Indicator position={position} />
    </View>
  );
}

export default Introduction;
