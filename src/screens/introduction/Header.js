import PropTypes from 'prop-types';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {screenHeight, screenWidth} from '../../Platforms';
import {vertical} from '../../Scales';
import dimensionsImage from './height_manager';

Header.propTypes = {
  imageHeader: PropTypes.number.isRequired,
  imageMain: PropTypes.number.isRequired,
  index: PropTypes.number,
};

Header.defaultProps = {
  imageHeader: 0,
  imageMain: 0,
  index: 1,
};

const ratioGuy = 102 / 178;

function Header({imageHeader, imageMain, index}) {
  return (
    <View
      style={[
        styles.header,
        {
          height:
            dimensionsImage[index].heightImageHeader +
            dimensionsImage[index].heightImageMain +
            (index === 0 ? vertical(20) : 0),
        },
      ]}>
      <FastImage
        source={imageHeader}
        style={{
          width: (screenHeight * 0.1) / ratioGuy,
          height: dimensionsImage[index].heightImageHeader,
        }}
        resizeMode={'cover'}
      />
      <FastImage
        source={imageMain}
        style={{
          marginTop: 20,
          width: dimensionsImage[index].widthImageMain,
          height: dimensionsImage[index].heightImageMain,
        }}
        resizeMode={'cover'}
      />
    </View>
  );
}

export default Header;

const styles = StyleSheet.create({
  header: {
    width: screenWidth,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
