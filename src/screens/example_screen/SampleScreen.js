import React, {useEffect, useCallback, useMemo, useState, useRef} from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, Text} from 'react-native';
import styles from './Sample.styles';
import {ViewPropTypes} from 'react-native';
import Constant from '../../Constant';

SampleScreen.propTypes = {};

SampleScreen.defaultProps = {};

function SampleScreen() {
  const isMounteRef = useRef(false);

  useEffect(() => {
    isMounteRef.current = true;
    if (isMounteRef.current) {
    }

    return () => {
      isMounteRef.current = false;
    };
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <Text>Sample Screen</Text>
    </SafeAreaView>
  );
}

export default React.memo(SampleScreen);
