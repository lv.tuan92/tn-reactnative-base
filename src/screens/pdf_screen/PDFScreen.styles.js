import {StyleSheet} from 'react-native';
import {screenHeight, screenWidth} from '../../Platforms';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewPdf: {
    flex: 1,
    width: screenWidth,
    height: screenHeight,
  },
});
