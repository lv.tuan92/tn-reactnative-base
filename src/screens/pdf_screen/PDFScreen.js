import React, {
  useEffect,
  useCallback,
  useMemo,
  useState,
  useLayoutEffect,
} from 'react';
import PropTypes from 'prop-types';
import {Platform, SafeAreaView, Text, View} from 'react-native';
import styles from './PDFScreen.styles';
import Pdf from 'react-native-pdf';

PDFScreen.propTypes = {};

PDFScreen.defaultProps = {};

function PDFScreen(props) {
  const {navigation, route} = props;
  /**
   * Load pdf local.
   * File pdf android bắt buộc phải đặt trong ../android/app/src/main/assets/test.pdf.
   */
  const source =
    Platform.OS === 'ios'
      ? require('../../../data_tutorial/test.pdf')
      : {uri: 'bundle-assets://test.pdf'};

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => <View />,
      title: 'Pdf',
    });
  }, [navigation]);

  return (
    <SafeAreaView style={styles.container}>
      <Pdf
        trustAllCerts={false}
        source={source}
        onLoadComplete={(numberOfPages, filePath) => {
          console.log(`Number of pages: ${numberOfPages}`);
        }}
        onPageChanged={(page, numberOfPages) => {
          console.log(`Current page: ${page}`);
        }}
        onError={error => {
          console.log(error);
        }}
        onPressLink={uri => {
          console.log(`Link pressed: ${uri}`);
        }}
        style={styles.viewPdf}
      />
    </SafeAreaView>
  );
}

export default React.memo(PDFScreen);
