import React, {useEffect, useCallback, useMemo, useState, useRef} from 'react';
import {RefreshControl, SafeAreaView, VirtualizedList} from 'react-native';
import styles from './HomeScreen.styles';
import {
  getPostAction,
  updateLoadingAction,
  updateShowDialogWarnAction,
} from '../../redux/actions';
import {useDispatch, useSelector} from 'react-redux';
import CMText from '../../components/CMText';
import {horizontal} from '../../Scales';
import {screenHeight} from '../../Platforms';
import {Color} from '../../helper/colors';
import ItemPost from './ItemPost';
import {getAllPost} from '../../api/crud';
import Constant from '../../Constant';

HomeScreen.propTypes = {};

HomeScreen.defaultProps = {};

function HomeScreen() {
  const dispatch = useDispatch();
  const [listPost, setListPost] = useState([]);
  const [isLoadMore, setLoadMore] = useState(false);
  const [isRefreshing, setRefreshing] = useState(false);
  const [offset, setOffset] = useState(0);
  const refVirtualizedList = useRef();
  const isMounteRef = useRef(false);

  useEffect(() => {
    isMounteRef.current = true;
    if (isMounteRef.current) {
      getPost();
    }

    return () => {
      isMounteRef.current = false;
    };
  }, []);

  /**
   *
   */
  const getPost = async () => {
    dispatch(updateLoadingAction(true));
    try {
      const response = await getAllPost();
      if (Constant.DATA_SUCCESS.includes(response?.status)) {
        setListPost(response.data);
      } else {
        dispatch(
          updateShowDialogWarnAction({
            isShowModalWarn: true,
            titleHeader: '',
            keyHeader: '',
            keyMessage: '',
            contentMessage: '',
          }),
        );
      }
      dispatch(updateLoadingAction(false));
    } catch (ex) {
      dispatch(
        updateShowDialogWarnAction({
          isShowModalWarn: true,
          titleHeader: '',
          keyHeader: '',
          keyMessage: '',
          contentMessage: '',
        }),
      );
      dispatch(updateLoadingAction(false));
    }
  };

  const handleLoadMore = () => {
    // if (!isLoadMore && !this.onEndReachedCalledDuringMomentum) {
    //   setLoadMore(true);
    //   //Get data.
    //   this.onEndReachedCalledDuringMomentum = true;
    // }
  };

  /**
   * On handle refresh.
   */
  const onRefresh = useCallback(() => {
    // setRefreshing(true);
    setOffset(0);
  }, []);

  const renderItem = useCallback(({item, index}) => {
    return <ItemPost item={item} key={index} />;
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <VirtualizedList
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={onRefresh}
            colors={[Color.base_color, Color.base_color]}
            tintColor={Color.base_color}
          />
        }
        onEndReached={() => {
          handleLoadMore();
        }}
        data={listPost}
        renderItem={renderItem}
        ref={refVirtualizedList}
        getItemCount={data => (data != undefined ? data.length : 0)}
        getItem={(data, index) => data[index]}
        keyExtractor={(item, index) => index.toString()}
        onMomentumScrollBegin={() => {
          // this.onEndReachedCalledDuringMomentum = false;
        }}
        removeClippedSubviews={true}
        maxToRenderPerBatch={10}
        onEndReachedThreshold={0.01}
        initialNumToRender={10}
        showsVerticalScrollIndicator={false}
      />
    </SafeAreaView>
  );
}

export default HomeScreen;
