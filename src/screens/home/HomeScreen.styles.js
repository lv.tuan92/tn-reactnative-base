import {StyleSheet} from 'react-native';
import {horizontal, vertical} from '../../Scales';
import {Color} from '../../helper/colors';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
