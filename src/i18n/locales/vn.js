export default {
  'languge-title-dialog': 'Chọn ngôn ngữ',
  'login-button': 'Đăng nhập',
  'sigup-button': 'Đăng ký',
  'get-start': 'Bắt đầu',
  'buton-diglog-language': 'Chọn',
  'log-out': 'Đăng xuất',
  'help-button': 'Trợ giúp',
  'text-change-password': 'Thay đổi mật khẩu',
  'text-help': 'Trợ giúp',
  'text-policy': 'Chính sách',
  'text-button-cancel': 'Huỷ',
  'text-button-submit': 'Đồng ý',
  'text-title-dialog-warn': 'Cảnh báo!',
  'text-loading': 'Đang tải...',
  'text-username-input': 'Tên đăng nhập',
  'text-password-input': 'Mật khẩu',
  'text-curent-password-input': 'Mật khẩu hiện tại',
  'text-new-password-input': 'Mật khẩu mới',
  'text-comfirm-password-input': 'Nhập lại mật khẩu mới',
  'text-forgot-password': 'Quên mật khẩu?',
  'text-do-not-account': 'Bạn chưa có tài khoản? ',
  'text-login-with-google': 'Đăng nhập với Google',
  'text-want-signout': 'Bạn chắc chắn muốn đăng xuất!',
  'text-notification': 'Thông báo!',
  'text-warning': 'Cảnh báo!',
  'text-regex-password': 'Ít nhất 8 ký tự với sự kết hợp của các chữ cái và số',
  'type_of_item': 'Bộ lọc tìm kiếm', 
  'delete_all_button_sheet': 'Xóa hết',
  'delete_button_sheet': 'Xóa',
  'text_sorted_by': 'Sắp xếp theo',
  'text_latest_creation_time': 'Thời gian tạo mới nhất',
  'text_highest_number_of_people': 'Số người tham gia học nhiều nhất',
  'text_btn_apply': 'Áp dụng',
  'text_list_question': 'Danh sách câu hỏi',



  
  

  /**
   * Text error.
   */
  'text-incorrect-password': 'Mật khẩu không chính xác',
  'text-empty-password': 'Cần nhập mật khẩu!',
  'text-empty-usename': 'Cần nhập tài khoản!',
  'text-sigin-serrver-error': 'Tài khoản hoặc mật khẩu sai, cần kiểm tra lại',

};
