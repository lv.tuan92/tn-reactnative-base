export default {
  'languge-title-dialog': 'Select language',
  'login-button': 'Sign In',
  'sigup-button': 'Sign Up',
  'get-start': 'Get start',
  'buton-diglog-language': 'Select',
  'log-out': 'Logout',
  'help-button': 'Help',
  'text-change-password': 'Change password',
  'text-help': 'Help',
  'text-policy': 'Policy',
  'text-button-cancel': 'Cancel',
  'text-button-submit': 'Submit',
  'text-title-dialog-warn': 'Warning!',
  'text-loading': 'Loading...',
  'text-username-input': 'User name',
  'text-password-input': 'Password',
  'text-curent-password-input': 'Curent password',
  'text-new-password-input': 'New password',
  'text-comfirm-password-input': 'Comfirm password',
  'text-forgot-password': 'Forgot password?',
  'text-do-not-account': 'Don`t have an accoount? ',
  'text-login-with-google': 'Login with Google',
  'text-want-signout': 'You definitely want to sign out!',
  'text-notification': 'Notification!',
  'text-warning': 'Warning!',
  'text-regex-password': 'Ít nhất 8 ký tự với sự kết hợp của các chữ cái và số',
  'type_of_item': 'Bộ lọc tìm kiếm', 
  'delete_all_button_sheet': 'Xóa hết',
  'delete_button_sheet': 'Xóa',
  'text_sorted_by': 'Sắp xếp theo',
  'text_latest_creation_time': 'Thời gian tạo mới nhất',
  'text_highest_number_of_people': 'Số người tham gia học nhiều nhất',
  'text_btn_apply': 'Áp dụng',
  'text_list_question': 'Danh sách câu hỏi',



  

  /**
   * Textinput error.
   */
  'text-incorrect-password': 'Incorrect password',
  'text-empty-password': 'Please enter password!',
  'text-empty-usename': 'Please enter usenmae!',
  'text-sigin-serrver-error': 'Account or password is wrong, need to check again',
};
