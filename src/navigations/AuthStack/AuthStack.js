import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Constant from '../../Constant';
import Introduction from '../../screens/introduction/Introduction';
import LoginScreen from '../../screens/login/LoginScreen';

const Author = createStackNavigator();

export const AuthStack = () => (
  <Author.Navigator>
    <Author.Screen
      name={Constant.INTRODUCTION}
      component={Introduction}
      options={{headerShown: false}}
    />
    <Author.Screen
      name={Constant.LOGIN}
      component={LoginScreen}
      options={{headerShown: false}}
    />
  </Author.Navigator>
);
