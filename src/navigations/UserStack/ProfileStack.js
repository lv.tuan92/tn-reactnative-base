import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Constant from '../../Constant';
import ProfileScreen from '../../screens/profile/ProfileScreen';
import useDefaultConfig from '../../CustomHooks/useDefaultConfig';
import {View} from 'react-native';
import PDFScreen from '../../screens/pdf_screen/PDFScreen';
import ChangePassword from '../../screens/change_password/ChangePassword';

const Profile = createStackNavigator();
export const ProfileStack = props => {
  const defaultConfig = useDefaultConfig(props);
  return (
    <Profile.Navigator
      initialRouteName={Constant.PROFILE_SCREEN}
      screenOptions={defaultConfig}>
      <Profile.Screen
        name={Constant.PROFILE_SCREEN}
        component={ProfileScreen}
        options={{headerShown: true, headerLeft: () => <View />}}
      />
      <Profile.Screen
        name={Constant.PDF_SCREEN}
        component={PDFScreen}
        options={{headerShown: true}}
      />
      <Profile.Screen
        name={Constant.CHANGE_PASSWORD_SCREEN}
        component={ChangePassword}
        options={{headerShown: true}}
      />
    </Profile.Navigator>
  );
};
