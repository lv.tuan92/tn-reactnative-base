import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Constant from '../../Constant';
import HomeScreen from '../../screens/home/HomeScreen';
import useDefaultConfig from '../../CustomHooks/useDefaultConfig';
import {View} from 'react-native';
import PDFScreen from '../../screens/pdf_screen/PDFScreen';

const Home = createStackNavigator();
export const HomeStack = props => {
  const defaultConfig = useDefaultConfig(props);
  return (
    <Home.Navigator
      initialRouteName={Constant.HOME_SCREEN}
      screenOptions={defaultConfig}>
      <Home.Screen
        name={Constant.HOME_SCREEN}
        component={HomeScreen}
        options={{headerShown: true, headerLeft: () => <View />}}
      />
      <Home.Screen
        name={Constant.PDF_SCREEN}
        component={PDFScreen}
        options={{headerShown: true}}
      />
    </Home.Navigator>
  );
};
