import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Constant from '../../Constant';
import {TabNavigation} from './TabNavigation';
import useDefaultConfig from '../../CustomHooks/useDefaultConfig';

const User = createStackNavigator();
export const UserStack = props => {
  const defaultConfig = useDefaultConfig(props);
  return (
    <User.Navigator screenOptions={defaultConfig}>
      <User.Screen
        name={Constant.BOTTOM_TAB}
        component={TabNavigation}
        options={{
          headerShown: false,
        }}
      />
    </User.Navigator>
  );
};
