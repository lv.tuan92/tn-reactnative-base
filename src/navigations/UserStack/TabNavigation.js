import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React, {useEffect, useState} from 'react';
import {Keyboard, Platform, StyleSheet, View} from 'react-native';
import {hasNotch} from 'react-native-device-info';
import {isIOS} from '../../Platforms';
import Constant from '../../Constant/index';
import globalStyles from '../../globalStyles/index';
import {ProfileStack} from './ProfileStack';
import HomeActiveIcon from "../../assets/home/home_active";
import HomeInActiveIcon from "../../assets/home/home_inactive";
import ProfileActiveIcon from "../../assets/profile/profile_active";
import ProfileInActiveIcon from "../../assets/profile/profile_inactive";
import IndicatorIcon from "../../assets/tabbar/indicator";
import {HomeStack} from './HomeStack';

const BottomTab = createBottomTabNavigator();

export const TabNavigation = props => {
  const [showTab, setShowTab] = useState(true);
  useEffect(() => {
    if (isIOS) return;
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setShowTab(false);
  };

  const _keyboardDidHide = () => {
    setShowTab(true);
  };

  return (
    <BottomTab.Navigator
      screenOptions={{
        tabBarHideOnKeyboard: true,
        tabBarActiveTintColor: '#6c99f5',
        tabBarAllowFontScaling: false,
        tabBarShowLabel: true,
        tabBarLabelStyle: {
          fontSize: 12,
          lineHeight: 14,
          marginBottom: hasNotch() && isIOS ? 15 : 5,
        },
        tabBarItemStyle: {
          backgroundColor: 'white',
          height: hasNotch() && isIOS ? 75 : 60,
        },
        tabBarStyle: showTab ? styles.tabbar : [styles.tabbar, {bottom: -75}],
      }}>
      <BottomTab.Screen
        name={Constant.HOMESTACK}
        component={HomeStack}
        options={{
          headerShown: false,
          tabBarLabel: Constant.HOME_TITLE_TAB,
          tabBarIcon: ({focused, color, size}) =>
            focused ? (
              <View style={globalStyles.containIconTab}>
                <IndicatorIcon />
                <HomeActiveIcon />
              </View>
            ) : (
              <View style={globalStyles.containIconTab}>
                <View style={globalStyles.emptyTabbar} />
                <HomeInActiveIcon />
              </View>
            ),
        }}
        listeners={({navigator, router}) => ({
          tabPress: e => {},
        })}
      />
      <BottomTab.Screen
        name={Constant.PROFILE_STACK}
        component={ProfileStack}
        options={{
          headerShown: false,
          tabBarLabel: Constant.PROFILE_TITLE_TAB,
          tabBarIcon: ({focused, color, size}) =>
            focused ? (
              <View style={globalStyles.containIconTab}>
                <IndicatorIcon />
                <ProfileActiveIcon />
              </View>
            ) : (
              <View style={globalStyles.containIconTab}>
                <View style={globalStyles.emptyTabbar} />
                <ProfileInActiveIcon />
              </View>
            ),
        }}
        listeners={({navigator, router}) => ({
          tabPress: e => {},
        })}
      />
    </BottomTab.Navigator>
  );
};

const styles = StyleSheet.create({
  tabbar: {
    backgroundColor: 'white',
    height: hasNotch() && isIOS ? 75 : 60,
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
      },
      android: {
        elevation: 2,
      },
    }),
    zIndex: 1,
  },
});
