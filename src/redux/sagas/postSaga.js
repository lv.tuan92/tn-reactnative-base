import {call, put, takeEvery, takeLatest} from 'redux-saga/effects';
import {GET_POST} from '../actions/types';
import {getAllPost} from '../../api/crud';
import {getPostActionError, getPostActionSuccess} from '../actions';

function* getPostSaga(actions) {
  try {
    const response = yield call(
      getAllPost,
      /**
       * Nếu api cần chuyền vào params thì viết sau func getAllPost.
       * vd:
       * action.payload.count,
       * action.payload.lenght
       */
    );
    yield put(getPostActionSuccess(response?.data));
  } catch (error) {
    yield put(getPostActionError(error));
  }
}

function* postSaga() {
  yield takeLatest(GET_POST, getPostSaga);
}

export default postSaga;
