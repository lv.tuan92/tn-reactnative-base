import {
  CLEAR_STORE,
  GET_POST,
  GET_POST_ERROR,
  GET_POST_SUCCESS,
  UPDATE_LANGUAGE,
  UPDATE_LOADING,
  UPDATE_SHOW_MODAL_WARN,
  UPDATE_USER,
} from './types';

export const updateLoadingAction = loading => {
  return {
    type: UPDATE_LOADING,
    payload: loading,
  };
};

export const updateShowDialogWarnAction = dialogWarning => {
  return {
    type: UPDATE_SHOW_MODAL_WARN,
    payload: dialogWarning,
  };
};

export const updateUserAction = user => {
  return {
    type: UPDATE_USER,
    payload: user,
  };
};

export const getPostAction = params => {
  return {
    type: GET_POST,
    payload: params,
  };
};

export const getPostActionSuccess = post => {
  return {
    type: GET_POST_SUCCESS,
    payload: post,
  };
};

export const getPostActionError = error => {
  return {
    type: GET_POST_ERROR,
    payload: error,
  };
};

export const updateLanguageAction = language => {
  return {
    type: UPDATE_LANGUAGE,
    payload: language,
  };
};

export const logoutAction = language => {
  return {
    type: CLEAR_STORE,
    payload: language,
  };
};
