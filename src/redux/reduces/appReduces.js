import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  UPDATE_LOADING,
  UPDATE_USER,
  ACCESS_TOKEN,
  UPDATE_LANGUAGE,
  GET_POST_SUCCESS,
  CLEAR_STORE,
  UPDATE_SHOW_MODAL_WARN,
} from '../actions/types';

const inittialState = {
  isLoading: false,
  token: null,
  userState: {
    signIn: false,
    userToken: null,
    user: null,
  },
  language: 'en',
  dialogWarning: {
    isShowModalWarn: false,
    isSigout: false,
    titleHeader: '',
    keyHeader: '',
    keyMessage: '',
    contentMessage: '',
  }
};

const appReduces = (state = inittialState, actions) => {
  switch (actions.type) {
    case UPDATE_LOADING:
      return {...state, isLoading: actions.payload};
    case UPDATE_SHOW_MODAL_WARN:
      return {...state, dialogWarning: actions.payload};
    case UPDATE_USER:
      return {...state, userState: actions.payload};
    case ACCESS_TOKEN:
      return {...state, token: actions.payload};
    case UPDATE_LANGUAGE:
      return {...state, language: actions.payload};
    case CLEAR_STORE:
      return {
        ...undefined,
        language: actions.payload,
      };
    default:
      return state;
  }
};

export default appReduces;
