import {getData} from '.';
import {updateShowDialogWarnAction} from '../redux/actions';
import {API_GET_POST} from './api';

export function getAllPost() {
  try {
    const response = getData(API_GET_POST);
    return response;
  } catch (error) {
    dispatch(
      updateShowDialogWarnAction({
        isShowModalWarn: true,
      }),
    );
  }
}
