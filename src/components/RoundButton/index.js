import PropTypes from 'prop-types';
import React from 'react';
import {View} from 'react-native';
import globalStyles from '../../globalStyles/index';
import TouchableDebounce from '../TouchableDebounce';

RoundButton.propTypes = {
  icon: PropTypes.node.isRequired,
};

RoundButton.defaultProps = {
  icon: <View />,
};

export default function RoundButton(props) {
  return (
    <TouchableDebounce
      {...props}
      style={[globalStyles.roundButton, props.style]}
      activeOpacity={0.8}>
      {props.icon}
    </TouchableDebounce>
  );
}
