import React from 'react';
import {StyleSheet, ViewPropTypes, View, Platform, Text} from 'react-native';
import TouchableDebounce from '../TouchableDebounce';
import IconNext1 from '../../assets/introduction/next_1.svg';
import IconNext2 from '../../assets/introduction/next_2.svg';
import {Color} from '../../helper/colors';
import {isIOS, screenWidth} from '../../Platforms';
import {horizontal, vertical} from '../../Scales';
import CMText from '../CMText';

CommonButton.propTypes = {
  style: ViewPropTypes.style,
};

CommonButton.defaultProps = {
  style: {},
};

function CommonButton({onPress, i18nKey, title, style, iconNext, iconAddress}) {
  const {width: btnCommonHoverWidth} = StyleSheet.flatten(style);
  return (
    <TouchableDebounce style={[styles.btnCommon, style]} onPress={onPress}>
      {isIOS ? (
        <View
          style={[
            styles.btnCommonHover,
            btnCommonHoverWidth
              ? {width: btnCommonHoverWidth - horizontal(20)}
              : undefined,
          ]}
        />
      ) : null}
      <CMText i18nKey={i18nKey} title={title} style={styles.textCommon} />
      {iconNext ? (
        <View
          style={{
            flexDirection: 'row',
            marginLeft: 5,
            paddingTop: 3,
          }}>
          <IconNext1 width={8} height={13} />
          <IconNext2 width={8} height={13} />
        </View>
      ) : null}
    </TouchableDebounce>
  );
}

const styles = StyleSheet.create({
  btnCommon: {
    paddingHorizontal: 15,
    height: vertical(50),
    width: '100%',
    backgroundColor: Color.base_color,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    elevation: 5,
  },
  btnCommonHover: {
    ...Platform.select({
      ios: {
        shadowColor: Color.base_color,
        shadowOpacity: 0.5,
        elevation: 5,
        shadowRadius: 10,
        shadowOffset: {width: 0, height: 10},
      },
    }),
    position: 'absolute',
    marginTop: 20,
    backgroundColor: Color.base_color,
    height: vertical(50),
    width: screenWidth - horizontal(30) - horizontal(20),
  },
  textCommon: {
    color: Color.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default CommonButton;
