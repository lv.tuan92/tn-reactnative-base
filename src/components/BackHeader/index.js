import {StackActions} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, Text} from 'react-native';
import IconBack from '../../assets/tabbar/iconback';
import {horizontal} from '../../Scales';
import TouchableDebounce from '../TouchableDebounce';
import CMText from '../CMText';

const BackHeader = ({dispatch, handleGoBack, title}) => {
  return (
    <TouchableDebounce
      style={styles.btnBack}
      onPress={() =>
        !handleGoBack ? dispatch(StackActions.pop(1)) : handleGoBack()
      }>
      <IconBack width={20} height={20} />
      <CMText i18nKey={title ? title : 'back_header'} style={styles.textBack} />
    </TouchableDebounce>
  );
};

const styles = StyleSheet.create({
  btnBack: {
    paddingLeft: horizontal(10),
    paddingRight: horizontal(10),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  textBack: {
    color: 'red',
    fontSize: 12,
    // marginLeft: horizontal(4),
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  iconBack: {
    width: 20,
    height: 20,
  },
});

export default BackHeader;
