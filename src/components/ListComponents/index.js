import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {VirtualizedList, StyleSheet, Text, View} from 'react-native';
import { useQuery } from '@tanstack/react-query';

function ListComponents() {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    const controller = new AbortController();
    const signal = controller.signal;
    const fetchData = async () => {
      try {
        const response = await axios.get(
          'https://jsonplaceholder.typicode.com/posts',
          {
            signal: signal,
          },
        );
        const data = response.data;
        setPosts(data);
      } catch (error) {
        if (axios.isCancel(error)) {
          // do something
        }
      }
    };
    fetchData();
    return () => {
      controller.abort();
    };
  }, []);

  /**
   * Render item post.
   * @param {*} param0
   * @returns
   */
  const ItemPost = ({item}) => {
    return (
      <View style={styles.viewItem}>
        <Text style={styles.textItem}>{`${item.id}. ${item.title}`}</Text>
        <Text style={styles.textItem}>{`${item.body}`}</Text>
      </View>
    );
  };

  const RenderItem = React.memo(({item}) => {
    return <ItemPost item={item} />;
  });

  /**
   * VirtualizedList load list data large custom.
   * Miss load refresh & load more data.
   */
  return (
    <VirtualizedList
      data={posts}
      renderItem={({item}) => <RenderItem item={item} key={item.id} />}
      keyExtractor={(item, index) => index.toString()}
      contentContainerStyle={styles.contentStylesList}
      getItemCount={data => (data != undefined ? data.length : 0)}
      getItem={(data, index) => data[index]}
      removeClippedSubviews={true}
      maxToRenderPerBatch={10}
    />
  );
}

const styles = StyleSheet.create({
  viewItem: {
    backgroundColor: '#212120',
    padding: 10,
    borderRadius: 10,
    marginTop: 10,
    marginHorizontal: 10,
  },
  textItem: {
    color: 'white',
    paddingVertical: 20,
  },
  contentStylesList: {
    padding: 0,
  },
});

export default ListComponents;
