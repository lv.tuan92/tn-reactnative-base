import React, {useContext} from 'react';
import {Modal, StyleSheet, View} from 'react-native';
import CMText from '../CMText';
import {Color} from '../../helper/colors';
import {horizontal, vertical} from '../../Scales';
import {screenWidth} from '../../Platforms';
import TouchableDebounce from '../TouchableDebounce';
import {useDispatch} from 'react-redux';
import {updateShowDialogWarnAction} from '../../redux/actions';
import {AuthContext} from '../../context/index';

function DialogWarn(props) {
  const {
    isShowModal,
    titleHeader,
    keyHeader,
    contentMessage,
    keyMessage,
    cancelOnPress,
    submitOnPress,
    isSigout = false,
  } = props;
  const dispatch = useDispatch();
  // const {signOut} = useContext(AuthContext);

  /**
   * Handle click button cancel.
   */
  const onHandleOnCancel = () => {
    dispatch(
      updateShowDialogWarnAction({
        isShowModalWarn: false,
      }),
    );
  };

  /**
   * Handle click button submit.
   */
  const onHandleOnSubmit = () => {
    if (isSigout) {
      // signOut();
    }
    dispatch(
      updateShowDialogWarnAction({
        isShowModalWarn: false,
      }),
    );
  };

  return (
    <Modal visible={isShowModal} transparent={true} animationType={'fade'}>
      <View style={styles.viewModal}>
        <View style={styles.viewContentDialog}>
          <View style={styles.viewHeader}>
            <CMText
              i18nKey={keyHeader ?? 'text-title-dialog-warn'}
              title={titleHeader}
              style={styles.textHeader}
            />
          </View>
          <View style={styles.viewContent}>
            <CMText
              i18nKey={keyMessage}
              title={contentMessage ?? 'Nội dung thông báo!!'}
              style={styles.textHeader}
            />
          </View>
          <View style={styles.viewLine} />
          <View style={styles.viewBottom}>
            <TouchableDebounce
              onPress={onHandleOnCancel}
              style={styles.viewButtonCancel}>
              <CMText
                i18nKey={'text-button-cancel'}
                style={styles.textCancel}
              />
            </TouchableDebounce>
            <TouchableDebounce
              onPress={onHandleOnSubmit}
              style={styles.viewButtonSubmit}>
              <CMText
                i18nKey={'text-button-submit'}
                style={styles.textSubmit}
              />
            </TouchableDebounce>
          </View>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  viewModal: {
    backgroundColor: Color.cl_background_dialog_warn,
    flex: 1,
    justifyContent: 'center',
  },
  viewContentDialog: {
    backgroundColor: Color.white,
    borderRadius: 8,
    width: '80%',
    alignSelf: 'center',
  },
  viewHeader: {
    marginVertical: vertical(10),
    alignItems: 'center',
  },
  textHeader: {
    fontWeight: 'bold',
  },
  viewLine: {
    height: 1,
    backgroundColor: Color.cl_border_input,
  },
  viewContent: {
    alignItems: 'center',
    marginTop: vertical(10),
    marginBottom: vertical(20),
  },
  viewBottom: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  viewButtonCancel: {
    width: '50%',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    height: vertical(40),
    borderRightWidth: 0.7,
    borderRightColor: Color.cl_border_input,
  },
  viewButtonSubmit: {
    width: '50%',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    height: vertical(40),
    borderLeftWidth: 0.7,
    borderLeftColor: Color.cl_border_input,
  },
  textCancel: {
    fontWeight: 'bold',
  },
  textSubmit: {
    color: Color.base_color,
    fontWeight: 'bold',
  },
});
export default DialogWarn;
