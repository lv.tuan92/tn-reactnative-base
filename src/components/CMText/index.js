import PropTypes from 'prop-types';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text} from 'react-native';
import {useSelector} from 'react-redux';
import {useComponentWillMount} from '../../CustomHooks/useComponentWillMount';
import I18n from '../../i18n/i18n';
import {Color} from '../../helper/colors';

CMText.propTypes = {
  i18nKey: PropTypes.string.isRequired,
  title: PropTypes.string,
  style: Text.propTypes.style,
};

CMText.defaultProps = {
  i18nKey: '',
  title: '',
  style: {},
};

export default function CMText(props) {
  const [i18n, setI18n] = useState(I18n);
  const language = useSelector(state => state.appReduces.language);

  useComponentWillMount(() => {
    setMainLocaleLanguage(language);
  });

  function setMainLocaleLanguage(language) {
    i18n.locale = language;
    setI18n(i18n);
  }

  useEffect(() => {
    setMainLocaleLanguage(language);
  }, [language]);

  const fontSize = props.style.fontSize || 14;

  return (
    <Text {...props} style={[styles.text, props.style]}>
      {props.i18nKey
        ? i18n.t(props.i18nKey).includes('missing')
          ? props.title
          : i18n.t(props.i18nKey)
        : props.title}
      {props.children}
    </Text>
  );
}

const styles = StyleSheet.create({
  text: {
    color: Color.text_color,
    fontSize: 16,
    lineHeight: 19,
  },
});
