import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { vertical } from "../../Scales";
import TouchableDebounce from "../TouchableDebounce";
import CMText from "../CMText";
import IconBack from "../../assets/component/icon_back.svg";
import IconNext from "../../assets/component/icon_next.svg";
import { screenWidth } from "../../Platforms";

function QuestionBottom(props) {
  const {
    countQuestion = 20,
    currentQuestion = 1,
    onHandleBack,
    onHandleNext,
  } = props;

  const [question, setQuestion] = useState(currentQuestion);

  const onPressBack = () => {
    if (question > 1) {
      setQuestion(question - 1);
    }
  };

  const onPressNext = () => {
    if (question < countQuestion) {
      setQuestion(question + 1);
    }
  };

  return (
    <View style={styles.container}>
      {question > 1 ? (
        <TouchableDebounce style={styles.button} onPress={onPressBack}>
          <IconBack width={6} height={12} />
        </TouchableDebounce>
      ) : (
        <View style={styles.button} />
      )}
      <CMText
        title={`${question}/${countQuestion} Câu hỏi`}
        style={styles.textContent}
      />
      {question < countQuestion ? (
        <TouchableDebounce style={styles.button} onPress={onPressNext}>
          <IconNext width={6} height={12} />
        </TouchableDebounce>
      ) : (
        <View style={styles.button} />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: vertical(100),
    width: "100%",
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "red",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    // position: 'absolute',
  },
  button: {
    width: screenWidth / 6,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
  },
  textContent: {
    width: (screenWidth * 2) / 3,
    textAlign: "center",
    fontSize: 14,
    color: "#0F172A",
  },
});
export default QuestionBottom;
