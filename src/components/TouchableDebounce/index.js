import PropTypes from "prop-types";
import React, { useCallback } from "react";
import { TouchableOpacity, ViewPropTypes } from "react-native";
const _ = require("lodash");

TouchableDebounce.propTypes = {
  onPress: PropTypes.func,
  style: ViewPropTypes.style,
  handleFirstTap: PropTypes.func,
  debounceTime: PropTypes.number,
};

TouchableDebounce.defaultProps = {
  style: {},
  handleFirstTap: () => null,
  debounceTime: 500,
};

export default function TouchableDebounce(props) {
  const _onPressFunc = () => {
    if (props.onPress) {
      props.onPress();
    }
  };

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      {...props}
      onPress={_.debounce(_onPressFunc, 500, {
        leading: true,
        trailing: false,
      })}
    >
      {props.children}
    </TouchableOpacity>
  );
}
