import PropTypes from 'prop-types';
import React, {useState, useRef} from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import globalStyles from '../../globalStyles';
import {Color} from '../../helper/colors';
import {screenWidth} from '../../Platforms';
import {horizontal, vertical} from '../../Scales';
import TouchableDebounce from '../TouchableDebounce';
import IconEyeOn from '../../assets/other/eye_on.svg';
import IconEyeOff from '../../assets/other/eye_on.svg';
import CMText from '../CMText';

CMTextInput.propTypes = {
  errorKey: PropTypes.string.isRequired,
  isValid: PropTypes.bool.isRequired,
  multiline: PropTypes.bool.isRequired,
  onChangeText: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.string,
  editable: PropTypes.bool,
  isEditUser: PropTypes.bool,
  isPassWord: PropTypes.bool,
  placeholder: PropTypes.string,
  maxLength: PropTypes.number,
};

CMTextInput.defaultProps = {
  errorKey: '',
  isValid: true,
  multiline: false,
  isEditUser: false,
  isPassWord: false,
  maxLength: 24,
};

function CMTextInput(props) {
  const {
    textKeyTitle,
    isInputRequied,
    multiline,
    isValid,
    errorKey,
    editable,
    textAlignVertical,
    isEditUser,
    isPassWord,
    inputRef,
    onShowPassword,
    placeholder,
    maxLength,
    keyboardType,
  } = props;

  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const _onChangeText = text => {
    if (props.onChangeText) {
      props.onChangeText(text);
    }
  };

  const _onBlur = text => {
    if (props.onBlur) {
      props.onBlur(text);
    }
  };
  return (
    <View style={styles.viewInput}>
      {textKeyTitle && (
        <View style={styles.viewTitleInput}>
          <CMText style={styles.titleViewInput} i18nKey={textKeyTitle} />
          {isInputRequied && <CMText style={styles.textVali} title={'*'} />}
        </View>
      )}
      <TextInput
        keyboardType={keyboardType}
        placeholder={placeholder}
        placeholderTextColor={Color.cl_text_place_holder}
        {...props}
        ref={inputRef}
        onBlur={_onBlur}
        style={{
          ...globalStyles.textInput,
          height: multiline ? 180 : 56,
          width: screenWidth - horizontal(15) * 2, //border text input = 1, only ip 7 plus.
          alignSelf: 'center',
          color: Color.text_color,
          borderColor: isValid
            ? Color.cl_border_text_input
            : Color.cl_text_requied,
          textAlignVertical: textAlignVertical,
          paddingEnd: isPassWord && !isEditUser ? horizontal(32) : 10,
          paddingStart: horizontal(15),
          ...props.style,
        }}
        maxLength={maxLength}
        selectionColor={Color.black}
        onChangeText={text => _onChangeText(text)}
        secureTextEntry={secureTextEntry}
      />
      {!isValid && <CMText style={styles.textRequied} i18nKey={errorKey} />}
      {isPassWord && !isEditUser ? (
        <TouchableDebounce
          onPress={() => {
            setSecureTextEntry(!secureTextEntry);
          }}
          style={styles.viewEyes}>
          {secureTextEntry ? (
            <IconEyeOff width={20} height={20} />
          ) : (
            <IconEyeOn width={20} height={20} />
          )}
        </TouchableDebounce>
      ) : null}
    </View>
  );
}
const styles = StyleSheet.create({
  viewInput: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    marginVertical: vertical(5),
  },
  viewTitleInput: {
    flexDirection: 'row',
    width: screenWidth - horizontal(15) * 2,
    alignSelf: 'center',
    marginTop: vertical(5),
  },
  titleViewInput: {
    fontSize: 16,
    lineHeight: 17,
    color: Color.text_color,
    fontWeight: 'bold',
  },
  textVali: {
    fontSize: 16,
    lineHeight: 17,
    color: Color.cl_requied_input,
    fontWeight: 'bold',
    marginBottom: 6,
    marginHorizontal: 3,
  },
  textRequied: {
    fontSize: 12,
    color: Color.cl_requied_input,
    width: screenWidth - horizontal(15) * 2,
    alignSelf: 'center',
  },
  viewEyes: {
    position: 'absolute',
    right: horizontal(22),
    width: 20,
    height: 20,
    top: vertical(35),
    justifyContent: 'center',
  },
});
export default CMTextInput;
