import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  Modal,
  FlatList,
} from "react-native";
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from "react-native-reanimated";
import { screenHeight, screenWidth } from "../../Platforms";
import { Color } from "../../helper/colors";
import { horizontal, vertical } from "../../Scales";
import CMText from "../CMText";
import TouchableDebounce from "../TouchableDebounce";
import IconCancel from "../../assets/component/icon_cancel.svg";

function BottomSheetQuestion(props) {
  const { isOpenModal, closeModal, countQuestion = 20 } = props;
  const translateY = useSharedValue(screenHeight);
  const opacity = useSharedValue(0);
  const [listQuestion, setListQuestion] = useState([]);
  useEffect(() => {
    if (isOpenModal) {
      translateY.value = withTiming(0, { duration: 500 }, () => {
        opacity.value = withTiming(1, { duration: 300 });
      });
    } else {
      opacity.value = withTiming(0, { duration: 300 }, () => {
        translateY.value = withTiming(screenHeight, { duration: 500 });
      });
    }
  }, [isOpenModal]);

  const translationStyles = useAnimatedStyle(() => ({
    transform: [{ translateY: translateY.value }],
  }));

  const opacityStyles = useAnimatedStyle(() => ({
    opacity: opacity.value,
  }));

  useEffect(() => {
    setListQuestion(generateArray(countQuestion));
  }, []);

  /**
   * Hàm gen ra mảng bằng tham số chuyền vào.
   * @returns
   */
  function generateArray(lenght) {
    return new Array(lenght).fill().map((_, index) => index);
  }

  const renderItem = ({ item }) => {
    return (
      <TouchableDebounce style={styles.viewItem}>
        <View style={styles.viewCircle} />
        <CMText title={item + 1} style={styles.textItem} />
      </TouchableDebounce>
    );
  };

  return (
    <Modal animationType="fade" transparent={true} visible={isOpenModal}>
      <Animated.View style={[styles.viewButtonSheet, translationStyles]}>
        <Animated.View
          style={[
            {
              ...StyleSheet.absoluteFillObject,
              backgroundColor: "#0000004D",
            },
            opacityStyles,
          ]}
        />
        <TouchableDebounce
          style={{
            ...StyleSheet.absoluteFillObject,
          }}
          activeOpacity={1}
        >
          <TouchableDebounce style={styles.btnCancel} onPress={closeModal}>
            <IconCancel width={24} height={24} />
          </TouchableDebounce>
          <TouchableWithoutFeedback>
            <View style={[styles.buttonSheet]}>
              <CMText style={styles.textTitle} i18nKey={"text_list_question"} />
              <FlatList
                data={listQuestion}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
                numColumns={5}
                contentContainerStyle={styles.flatlist}
              />
            </View>
          </TouchableWithoutFeedback>
        </TouchableDebounce>
      </Animated.View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  viewButtonSheet: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonSheet: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: Color.white,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    paddingBottom: vertical(14),
  },
  btnCancel: {
    width: 48,
    height: 48,
    alignSelf: "center",
    borderRadius: 24,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 370,
  },
  textTitle: {
    fontSize: 20,
    color: "#0F172A",
    fontWeight: "700",
    lineHeight: 28,
    paddingHorizontal: horizontal(14),
    paddingTop: vertical(20),
    paddingBottom: vertical(15),
  },
  flatlist: {
    justifyContent: "flex-start", // Start from the beginning
    alignItems: "flex-start", // Start from the beginning
    paddingHorizontal: 2, // Adjust as needed
    backgroundColor: "white",
  },
  viewItem: {
    borderWidth: 1,
    borderColor: "#CBD5E1",
    borderRadius: 8,
    width: 52.6,
    height: 43,
    margin: (screenWidth - 52.6 * 5 - 10) / 10,
    justifyContent: "center",
    alignItems: "center",
  },
  textItem: {
    fontSize: 14,
    color: "#0F172A",
    fontWeight: "400",
  },
  viewCircle: {
    position: "absolute",
    top: 0,
    left: 0,
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid",
    borderLeftWidth: 18.47,
    borderBottomWidth: 18.5,
    borderLeftColor: "transparent",
    borderBottomColor: "#FBC53E",
    transform: [{ rotate: "180deg" }],
    borderBottomRightRadius: 8,
  },
});
export default BottomSheetQuestion;
