import React from 'react';
import {useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {horizontal, vertical} from '../../Scales';
import {useEffect} from 'react';
import IconRegex from '../../assets/login/icon_regex_pass.svg';
import IconUnRegex from '../../assets/login/icon_unregex_pass.svg';
import CMText from '../CMText';

function RegexPassword(props) {
  /**
   * lenghtMax: chiều dài của tổng regex.
   * passRegex: tổng số regex item đã pass.
   */
  const {lenghtMax, passRegex} = props;
  const [listItem, setListItem] = useState([]);

  useEffect(() => {
    setListItem(generateArray(lenghtMax));
  }, [lenghtMax]);

  /**
   * Hàm gen ra mảng bằng tham số chuyền vào.
   * @returns
   */
  function generateArray(lenght) {
    return new Array(lenght).fill().map((_, index) => index);
  }

  /**
   * Render item
   */
  const Item = ({item, index}) => {
    return (
      <View
        style={[
          styles.viewItemUnPass,
          {
            backgroundColor: index + 1 <= passRegex ? '#0056D2' : '#F1F5F9',
          },
        ]}
      />
    );
  };
  return (
    <View styles={styles.container}>
      {passRegex > 0 && (
        <FlatList
          data={listItem}
          renderItem={({item, index}) => <Item item={item} index={index} />}
          keyExtractor={item => item.id}
          horizontal={true}
          scrollEnabled={false}
        />
      )}
      <View style={styles.viewContextRegex}>
        {passRegex > 3 ? (
          <IconRegex width={18} height={18} />
        ) : (
          <IconUnRegex width={18} height={18} />
        )}
        <CMText
          i18nKey={'text-regex-password'}
          style={[
            styles.textContextRegex,
            {color: passRegex > 3 ? '#23AA26' : '#64748B'},
          ]}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
    paddingHorizontal: horizontal(10),
  },
  viewItemUnPass: {
    width: horizontal(40),
    height: 3,
    borderRadius: 2,
    marginLeft: horizontal(5),
    marginVertical: vertical(10),
    backgroundColor: '#F1F5F9',
  },
  viewContextRegex: {
    flexDirection: 'row',
  },
  textContextRegex: {
    fontSize: 12,
    marginLeft: horizontal(5),
  },
});
export default RegexPassword;
