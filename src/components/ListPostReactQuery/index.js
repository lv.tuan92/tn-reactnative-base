import React from 'react';
import {View, StyleSheet, FlatList, TouchableOpacity, Text} from 'react-native';
import usePosts from '../../hooks/usePosts';
import {Color} from '../../helper/colors';
import {LoadingIndicator} from '../LoadingIndicator';
import IconAdd from '../../assets/icons/icon_add.svg';
import HomeActiveIcon from '../../assets/icons/home_active';

function ListPostReactQuery() {
  const {data, isLoading, isSuccess} = usePosts();

  return (
    <View style={styles.container}>
      {isLoading && (
        <View>
          <Text style={styles.loading}>Loading...</Text>
          <IconAdd width={15} height={15} />
          <LoadingIndicator />
        </View>
      )}
      {isSuccess && (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data}
          style={styles.wrapper}
          keyExtractor={item => `${item.id}`}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => {}} style={styles.post}>
              <View style={styles.item}>
                <Text style={styles.postTitle}>{item.title}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
      )}
    </View>
  );
}

export default ListPostReactQuery;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loading: {
    alignSelf: 'center',
  },
  wrapper: {
    flex: 1,
    paddingVertical: 30,
  },
  item: {
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  header: {
    textAlign: 'center',
    textTransform: 'capitalize',
    fontWeight: 'bold',
    fontSize: 30,
    color: Color.primary,
    paddingVertical: 10,
  },
  post: {
    backgroundColor: Color.primary,
    padding: 15,
    borderRadius: 10,
    marginBottom: 20,
  },
  postTitle: {
    color: 'white',
    textTransform: 'capitalize',
  },
});
