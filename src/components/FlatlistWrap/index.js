import React from 'react';
import {StyleSheet, View, FlatList, Text} from 'react-native';
import CMText from '../CMText';
import { horizontal } from '../../Scales';
import TouchableDebounce from '../TouchableDebounce';

const data = [
  {key: '1', value: 'Kinh doanh'},
  {key: '2', value: 'Kỹ năng mềm'},
  {key: '3', value: 'Thuế nhà'},
  {key: '4', value: 'Kỹ năng mềm'},
  {key: '5', value: 'Kỹ năng mềm'},
  {key: '6', value: 'actor'},
  {key: '7', value: 'Kỹ năng'},
  {key: '8', value: 'Kinh doanh'},
  {key: '9', value: 'Giao tiếp'},
  {key: '10', value: 'Kỹ năng mềm'},
  {key: '11', value: 'Kỹ năng mềm'},
];
function FlatlistWrap(props) {
  const {listData = data} = props;
  const renderItem = ({item}) => (
    <TouchableDebounce style={styles.viewItem}>
      <CMText title={item?.value} style={styles.textItem}/>
    </TouchableDebounce>
  );

  return (
    <View>
      <FlatList
        data={listData}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        numColumns={3} 
        contentContainerStyle={styles.list}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  list: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  viewItem: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F1F5F9',
    margin: '1.5%',
    height: 36,
    borderRadius: 18,
    paddingHorizontal: horizontal(10),
    borderWidth: 1,
    borderColor: '#E2E8F0'
  },
  textItem: {
    fontSize: 12,
    color: '#0F172A',
    lineHeight: 20.4,
  }
});

export default FlatlistWrap;
