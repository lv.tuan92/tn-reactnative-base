import React, {useEffect} from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  Modal,
} from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import {screenHeight} from '../../Platforms';
import {Color} from '../../helper/colors';
import {horizontal, vertical} from '../../Scales';
import CMText from '../CMText';
import TouchableDebounce from '../TouchableDebounce';
import ItemSwitchDots from '../ItemSwitchDots';
import IconCancel from '../../assets/component/icon_cancel.svg';

function BottomSheetFilter(props) {
  const {isOpenModal, closeModal, handleApplyOnPress} = props;
  const translateY = useSharedValue(screenHeight);
  const opacity = useSharedValue(0);
  useEffect(() => {
    if (isOpenModal) {
      translateY.value = withTiming(0, {duration: 500}, () => {
        opacity.value = withTiming(1, {duration: 300});
      });
    } else {
      opacity.value = withTiming(0, {duration: 300}, () => {
        translateY.value = withTiming(screenHeight, {duration: 500});
      });
    }
  }, [isOpenModal]);

  const translationStyles = useAnimatedStyle(() => ({
    transform: [{translateY: translateY.value}],
  }));

  const opacityStyles = useAnimatedStyle(() => ({
    opacity: opacity.value,
  }));

  /**
   *
   * @param {*} i18keyContext : key của text content.
   * @param {*} i18KeyDelete : key của button delete.
   * @param {*} type : check fontSize của text title.
   * @returns
   */
  const RenderViewTitle = ({i18keyContext, i18KeyDelete, type}) => {
    return (
      <View style={styles.viewHeader}>
        <CMText
          i18nKey={i18keyContext}
          style={[
            styles.textTitle,
            {
              fontSize: type == 1 ? 20 : 18,
            },
          ]}
        />
        <TouchableDebounce>
          <CMText i18nKey={i18KeyDelete} style={[styles.textDelete]} />
        </TouchableDebounce>
      </View>
    );
  };

  return (
    <Modal animationType="fade" transparent={true} visible={isOpenModal}>
      <Animated.View style={[styles.viewButtonSheet, translationStyles]}>
        <Animated.View
          style={[
            {
              ...StyleSheet.absoluteFillObject,
              backgroundColor: '#0000004D',
            },
            opacityStyles,
          ]}
        />
        <TouchableDebounce
          style={{
            ...StyleSheet.absoluteFillObject,
          }}
          activeOpacity={1}>
          <TouchableDebounce style={styles.btnCancel} onPress={closeModal}>
            <IconCancel width={24} height={24} />
          </TouchableDebounce>
          <TouchableWithoutFeedback>
            <View style={[styles.buttonSheet]}>
              <ScrollView
                scrollEnabled={true}
                bounces={false}
                showsVerticalScrollIndicator={false}
                style={styles.scrollView}>
                <RenderViewTitle
                  i18keyContext={'type_of_item'}
                  i18KeyDelete={'delete_all_button_sheet'}
                  type={1}
                />
                {/** 1 */}
                <RenderViewTitle
                  i18keyContext={'text_sorted_by'}
                  i18KeyDelete={'delete_button_sheet'}
                  type={2}
                />
                <ItemSwitchDots
                  i18nKeyContext={'text_latest_creation_time'}
                  type={1}
                  status={false}
                  containerStyle={styles.viewDots}
                />
                <ItemSwitchDots
                  i18nKeyContext={'text_highest_number_of_people'}
                  type={2}
                  status={false}
                  containerStyle={styles.viewDots}
                />
                <View style={styles.viewLine} />
                {/** 2 */}
                <RenderViewTitle
                  i18keyContext={'text_sorted_by'}
                  i18KeyDelete={'delete_button_sheet'}
                  type={2}
                />
                <ItemSwitchDots
                  i18nKeyContext={'text_latest_creation_time'}
                  type={1}
                  status={false}
                  containerStyle={styles.viewDots}
                />
                <ItemSwitchDots
                  i18nKeyContext={'text_highest_number_of_people'}
                  type={2}
                  status={false}
                  containerStyle={styles.viewDots}
                />
                <View style={styles.viewLine} />
                {/** 3 */}
                <RenderViewTitle
                  i18keyContext={'text_sorted_by'}
                  i18KeyDelete={'delete_button_sheet'}
                  type={2}
                />
                <ItemSwitchDots
                  i18nKeyContext={'text_latest_creation_time'}
                  type={1}
                  status={false}
                  containerStyle={styles.viewDots}
                />
                <ItemSwitchDots
                  i18nKeyContext={'text_highest_number_of_people'}
                  type={2}
                  status={false}
                  containerStyle={styles.viewDots}
                />
                <View style={styles.viewLine} />
                <TouchableDebounce style={styles.btnApply} onPress={handleApplyOnPress}>
                  <CMText
                    i18nKey={'text_btn_apply'}
                    style={styles.textBtnApply}
                  />
                </TouchableDebounce>
              </ScrollView>
            </View>
          </TouchableWithoutFeedback>
        </TouchableDebounce>
      </Animated.View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  viewButtonSheet: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonSheet: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: Color.white,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    height: screenHeight / 2,
    paddingVertical: 24,
  },
  viewHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: vertical(14),
    paddingHorizontal: vertical(14),
  },
  textTitle: {
    fontSize: 20,
    color: '#0F172A',
    fontWeight: '700',
  },
  textDelete: {
    fontWeight: '400',
    color: '#0056D2',
    fontSize: 12,
  },
  scrollView: {
    // backgroundColor: 'red',
    height: screenHeight / 2,
  },
  viewBody: {
    marginTop: vertical(15),
    paddingHorizontal: horizontal(20),
  },
  textItem: {
    fontSize: 16,
    color: Color.cl_text_app,
  },

  btnCancel: {
    width: 48,
    height: 48,
    alignSelf: 'center',
    borderRadius: 24,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    top: screenHeight / 2 - 60,
  },
  viewLine: {
    height: vertical(10),
    width: '100%',
    backgroundColor: '#F1F5F9',
  },
  viewDots: {
    paddingHorizontal: vertical(14),
  },
  btnApply: {
    backgroundColor: '#0056D2',
    height: 56,
    marginHorizontal: horizontal(14),
    borderRadius: 28,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBtnApply: {
    fontSize: 16,
    fontWeight: '700',
    color: '#FFFFFF',
  },
});

export default BottomSheetFilter;
