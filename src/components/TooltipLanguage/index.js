import React from 'react';
import {StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import Tooltip from 'react-native-walkthrough-tooltip';
import {Color} from '../../helper/colors';
import TouchableDebounce from '../TouchableDebounce';
import CMText from '../CMText';
import FastImage from 'react-native-fast-image';
import {useState} from 'react';
import {useCallback} from 'react';
import {vertical} from '../../Scales';
import {updateLanguageAction} from '../../redux/actions';
import {useDispatch, useSelector} from 'react-redux';

function TooltipLanguage(props) {
  const dispatch = useDispatch();
  const languageLocal = useSelector(state => state.appReduces.language);
  const {toolTipVisible, openTooltip, colseTooltip} = props;
  const [language, setLanguage] = useState([
    {
      id: 1,
      title: 'English',
      value: 'en',
      icon: require('../../assets/other/check.png'),
      isSelect: true,
    },
    {
      id: 2,
      title: 'Việt Nam',
      value: 'vn',
      icon: require('../../assets/other/check.png'),
      isSelect: false,
    },
  ]);

  /**
   * Handler select language.
   */
  const handleSelectLanguage = useCallback(items => {
    setLanguage(prevItems =>
      prevItems.map((item, index) => {
        return item.id !== items.id
          ? {...item, isSelect: false}
          : {...item, isSelect: true};
      }),
    );
    dispatch(updateLanguageAction(items.value));
    colseTooltip();
  }, []);

  return (
    <Tooltip
      isVisible={toolTipVisible}
      content={
        <View style={styles.viewContent}>
          <CMText i18nKey={'languge'} style={styles.textTitle} />
          <View style={[styles.viewLine]} />
          {language.map((item, index) => {
            return (
              <TouchableDebounce
                key={item.id}
                style={styles.btnLanguage}
                onPress={() => {
                  handleSelectLanguage(item);
                }}>
                <Text
                  style={[
                    styles.textItemLanguage,
                    {
                      color: item.isSelect
                        ? Color.base_color
                        : Color.text_color,
                    },
                  ]}>
                  {item.title}
                </Text>
                {item.isSelect && (
                  <FastImage
                    source={item.icon}
                    resizeMode={'contain'}
                    style={styles.iconCheck}
                  />
                )}
              </TouchableDebounce>
            );
          })}
          <View style={styles.viewLine} />
        </View>
      }
      onClose={() => colseTooltip()}
      placement={'center'}>
      <TouchableDebounce
        style={styles.buttonLanguage}
        onPress={() => openTooltip()}>
        <FastImage
          resizeMode={'contain'}
          source={
            languageLocal === 'vn'
              ? require('../../assets/language/vn.png')
              : require('../../assets/language/en.png')
          }
          style={styles.iamgeLanguage}
        />
      </TouchableDebounce>
    </Tooltip>
  );
}

export default React.memo(TooltipLanguage);

const styles = StyleSheet.create({
  buttonLanguage: {},
  viewLine: {
    height: 0.5,
    backgroundColor: Color.base_color,
  },
  textTitle: {
    color: Color.text_color,
    fontSize: 15,
    marginVertical: 10,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  textLanguage: {
    color: Color.white,
    fontSize: 15,
  },
  viewContent: {
    width: 200,
    height: vertical(100),
  },
  btnLanguage: {
    height: 40,
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
  },
  textItemLanguage: {
    color: Color.text_color,
    fontSize: 16,
  },
  iconCheck: {
    width: 15,
    height: 15,
  },
  bntStart: {
    backgroundColor: Color.base_color,
    marginTop: 15,
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  iamgeLanguage: {
    width: 30,
    height: 25,
  },
});
