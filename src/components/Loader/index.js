import React from 'react';
import {ActivityIndicator, Image, Modal, StyleSheet, View} from 'react-native';
import {vertical} from '../../Scales';
import FastImage from 'react-native-fast-image';
import CMText from '../CMText';
import { Color } from '../../helper/colors';

function Loader(props) {
  const {isLoading} = props;
  return (
    <Modal visible={isLoading} transparent={true}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <FastImage
            style={styles.imageGif}
            source={require('../../assets/other/image_loading.gif')}
          />
          <CMText i18nKey={'text-loading'} />
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: Color.background_dialog_loading,
  },
  activityIndicatorWrapper: {
    backgroundColor: Color.white,
    height: vertical(100),
    width: vertical(100),
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  imageGif: {
    height: vertical(50),
    width: vertical(50),
  },
});
export default Loader;
