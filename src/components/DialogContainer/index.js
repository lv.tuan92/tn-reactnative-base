import React, {useEffect} from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  Modal,
  TouchableOpacity,
  Text,
} from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import {useDispatch, useSelector} from 'react-redux';
import { screenHeight, screenWidth } from '../../Platforms';
import { Color } from '../../helper/colors';


function DialogContainer(props) {
  const {isOpenModal, closeModal, textButton, textContent} = props;
  const dispatch = useDispatch();
  const translateY = useSharedValue(screenHeight);
  const opacity = useSharedValue(0);

  useEffect(() => {
    if (isOpenModal) {
      translateY.value = withTiming(0, {duration: 500}, () => {
        opacity.value = withTiming(1, {duration: 300});
      });
    } else {
      opacity.value = withTiming(0, {duration: 300}, () => {
        translateY.value = withTiming(screenHeight, {duration: 500});
      });
    }
  }, [isOpenModal]);

  const translationStyles = useAnimatedStyle(() => ({
    transform: [{translateY: translateY.value}],
  }));

  const opacityStyles = useAnimatedStyle(() => ({
    opacity: opacity.value,
  }));

  /**
   * Close bottom call.
   */
  const closeModalCall = () => {
    closeModal();
  };


  return (
    <Modal animationType="fade" transparent={true} visible={isOpenModal}>
      <Animated.View style={[styles.viewButtonSheet, translationStyles]}>
        <Animated.View
          style={[
            {
              ...StyleSheet.absoluteFillObject,
              backgroundColor: Color.bg_buttom_sheet,
            },
            opacityStyles,
          ]}
        />
        <TouchableOpacity
          style={{
            ...StyleSheet.absoluteFillObject,
          }}
          activeOpacity={1}
          onPress={closeModalCall}>
          <TouchableWithoutFeedback>
            <View style={[styles.buttonSheet]}>
              <Text>dsdfdfdf</Text>
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Animated.View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  imageButtonSheet: {
    width: screenWidth - 88 * 2, //180,
    height: 160,
    alignSelf: 'center',
    marginTop: 40,
  },
  textButtonSheet: {
    marginTop: 38,
    marginBottom: 15,
    fontSize: 18,
    color: Color.text_color,
    alignSelf: 'center',
    textAlign: 'center',
  },
  viewButtonSheet: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonSheet: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: Color.white,
    paddingTop: 10,
    borderTopLeftRadius: 22,
    borderTopRightRadius: 22,
  },
  viewWarning: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: Color.base_color,
    alignSelf: 'center',
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnConfirm: {
    backgroundColor: Color.base_color,
    width: screenWidth / 2 - 15 * 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textConfirm: {
    paddingVertical: 19,
    fontSize: 18,
    color: Color.white,
  },
  image: {
    width: screenWidth - 60,
    height: screenWidth - 60,
    alignSelf: 'center',
  },
  buttonGotoLogin: {
    height: 50,
    width: '100%',
    backgroundColor: Color.base_color,
    borderRadius: 5,
    alignSelf: 'center',
    justifyContent: 'center',
    marginVertical: 40,
  },
  textButton: {
    color: Color.white,
    justifyContent: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default React.memo(DialogContainer);
