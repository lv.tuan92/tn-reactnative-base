import React from 'react';
import {StyleSheet, View} from 'react-native';
import SwitchToggle from 'react-native-switch-toggle';
import CMText from '../CMText';
import {useState} from 'react';
import IconSwitch from '../../assets/component/check_switch.svg';
import TouchableDebounce from '../TouchableDebounce';
import { vertical } from '../../Scales';

const TYPE_DOTS = 1;
const TYPE_SWITCH = 2;

function ItemSwitchDots(props) {
  /**
   * textItem: Nội dung của text.
   * type: loại switch toggle hay là dots.
   * trạng thái on hay off.
   */
  const {textItem, type, status, i18nKeyContext, containerStyle} = props;
  const [on, setOn] = useState(status);

  /**
   * Handle onpress swicth toggle.
   */
  const onPressSwitch = () => {
    setOn(!on);
  };
  return (
    <View style={[styles.container, containerStyle]}>
      <CMText style={styles.textContext} i18nKey={i18nKeyContext} />
      {type == TYPE_DOTS ? (
        <TouchableDebounce
          onPress={onPressSwitch}
          style={[
            styles.btnDots,
            {
              borderWidth: on ? 7 : 1,
              borderColor: on ? '#0153FF' : '#CBD5E1',
            },
          ]}
        />
      ) : (
        <SwitchToggle
          switchOn={on}
          onPress={onPressSwitch}
          circleColorOff="#FFFFFF"
          circleColorOn="#FFFFFF"
          backgroundColorOn="#0153FF"
          backgroundColorOff="#E2E8F0"
          containerStyle={styles.containerStyle}
          circleStyle={styles.circleStyle}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: vertical(10),
  },
  containerStyle: {
    width: 44,
    height: 28,
    borderRadius: 22,
    padding: 2,
  },
  circleStyle: {
    width: 24,
    height: 24,
    borderRadius: 12,
  },
  textContext: {
    fontSize: 14,
    color: '#0F172A',
    fontWeight: '400'
  },
  btnDots: {
    width: 24,
    height: 24,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: '#CBD5E1',
    backgroundColor: '#FFFFFF',
  },
});
export default ItemSwitchDots;
