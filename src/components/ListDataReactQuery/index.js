import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {
  VirtualizedList,
  StyleSheet,
  Text,
  View,
  RefreshControl,
} from 'react-native';
import {useQuery} from '@tanstack/react-query';
import {fetchMovies} from '../../api';
import {useRefreshByUser} from '../../hooks/useRefreshByUser';
import {useRefreshOnFocus} from '../../hooks/useRefreshOnFocus';
import {LoadingIndicator} from '../LoadingIndicator';

function ListDataReactQuery() {
  const {data, isLoading, error, refetch} = useQuery(['movies'], fetchMovies);
  const {isRefetchingByUser, refetchByUser} = useRefreshByUser(refetch);
  useRefreshOnFocus(refetch);

  /**
   * Render item.
   * @param {*} param0
   * @returns
   */
  const ItemPost = ({item}) => {
    return (
      <View style={styles.viewItem}>
        <Text style={styles.textItem}>{`${item.year} - ${item.title}`}</Text>
        {/* <Text style={styles.textItem}>{`${item.body}`}</Text> */}
      </View>
    );
  };

  const RenderItem = React.memo(({item}) => {
    return <ItemPost item={item} />;
  });

  if (isLoading) return <LoadingIndicator />;
  /**
   * VirtualizedList load list data large custom.
   * Miss load refresh & load more data.
   */
  return (
    <VirtualizedList
      data={data}
      renderItem={({item}) => <RenderItem item={item} key={item.id} />}
      keyExtractor={(item, index) => index.toString()}
      contentContainerStyle={styles.contentStylesList}
      getItemCount={data => (data != undefined ? data.length : 0)}
      getItem={(data, index) => data[index]}
      removeClippedSubviews={true}
      maxToRenderPerBatch={10}
      refreshControl={
        <RefreshControl
          refreshing={isRefetchingByUser}
          onRefresh={refetchByUser}
        />
      }
    />
  );
}

const styles = StyleSheet.create({
  viewItem: {
    backgroundColor: '#212120',
    padding: 10,
    borderRadius: 10,
    marginTop: 10,
    marginHorizontal: 10,
  },
  textItem: {
    color: 'white',
    paddingVertical: 20,
  },
  contentStylesList: {
    padding: 0,
  },
});

export default ListDataReactQuery;
