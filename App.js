import React, {useEffect, useRef} from 'react';
import {StyleSheet, LogBox} from 'react-native';
import {QueryClient, QueryClientProvider} from '@tanstack/react-query';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Constant from './src/Constant';
import {AuthStack} from './src/navigations/AuthStack/AuthStack';
import {UserStack} from './src/navigations/UserStack/UserStack';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useMemo} from 'react';
import {AuthContext} from './src/context/index';
import {
  logoutAction,
  updateLanguageAction,
  updateShowDialogWarnAction,
  updateUserAction,
} from './src/redux/actions';
import SplashScreen from 'react-native-splash-screen';
import {isReadyRef, navigationRef} from './src/navigations/RootNavigation';
import DialogWarn from './src/components/DialogWarn';
import Loader from './src/components/Loader';
import {GoogleSignin} from '@react-native-google-signin/google-signin';

LogBox.ignoreAllLogs(true);
LogBox.ignoreLogs(['EventEmitter.removeListener']);
LogBox.ignoreLogs(['Require cycle:']);
LogBox.ignoreLogs([
  'ViewPropTypes will be removed',
  'ColorPropType will be removed',
]);

const RootStack = createStackNavigator();
const App = () => {
  const queryClient = new QueryClient({
    defaultOptions: {queries: {retry: 2}},
  });
  const dispatch = useDispatch();
  const userState = useSelector(state => state.appReduces.userState);
  const languageLocal = useSelector(state => state.appReduces.language);
  const dialogWarning = useSelector(state => state.appReduces.dialogWarning);
  const isLoading = useSelector(state => state.appReduces.isLoading);
  const delaySplash = useRef(null);

  const authContext = useMemo(
    () => ({
      signIn: async data => {
        /**
         * Muốn app lưu quá trình đăng nhập.
         * Cần lưu token trong storage.
         * Sau đó call api get data use.
         * Sau khi call thành công thì update data user lại vào trong store redux.
         */
        if (data) {
          await AsyncStorage.setItem(Constant.DATA_USER, data.userToken);
          dispatch(updateUserAction(data));
        }
      },
      signUp: async data => {
        await AsyncStorage.setItem(Constant.DATA_USER, data.userToken);
      },
      signOut: async () => {
        const language = await AsyncStorage.getItem(Constant.LANGUAGE_APP);
        dispatch(logoutAction(language));
        try {
          await GoogleSignin.revokeAccess();
          await GoogleSignin.signOut();
          // await auth().signOut();
          await AsyncStorage.removeItem(Constant.DATA_USER);
        } catch (e) {}
      },
    }),
    [],
  );

  /**
   * Inittial data language for app.
   * Login app lần đầu thì gán bằng type language trong store.
   * Login các lần tiếp theo thì lấy type trong async storage.
   */
  useEffect(() => {
    const initLanguage = async () => {
      const languages = await AsyncStorage.getItem(Constant.LANGUAGE_APP);
      if (languages == null || languages == undefined) {
        await AsyncStorage.setItem(Constant.LANGUAGE_APP, languageLocal);
      } else {
        dispatch(updateLanguageAction(languages));
      }
    };

    initLanguage();
  }, []);

  useEffect(() => {
    const getAccessToken = async () => {
      delaySplash.current = setTimeout(() => {
        SplashScreen.hide();
      }, 3000);
    };
    getAccessToken();
    return () => {
      clearTimeout(delaySplash.current);
    };
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <NavigationContainer
        ref={navigationRef}
        onReady={() => {
          isReadyRef.current = true;
        }}>
        <AuthContext.Provider value={authContext}>
          <RootStack.Navigator initialRouteName={Constant.INTRODUCTION}>
            {userState?.signIn ? (
              <RootStack.Screen
                name={Constant.USER_STACK}
                component={UserStack}
                options={{headerShown: false}}
              />
            ) : (
              <RootStack.Screen
                name={Constant.AUTHOR_STACK}
                component={AuthStack}
                options={{headerShown: false}}
              />
            )}
          </RootStack.Navigator>
        </AuthContext.Provider>
        {dialogWarning?.isShowModalWarn && (
          <DialogWarn
            isShowModal={dialogWarning?.isShowModalWarn}
            titleHeader={dialogWarning?.titleHeader}
            keyHeader={dialogWarning?.keyHeader}
            contentMessage={dialogWarning?.keyHeader}
            keyMessage={dialogWarning?.keyMessage}
            isSigout={dialogWarning?.isSigout}
          />
        )}
        {isLoading && <Loader isLoading={isLoading} />}
      </NavigationContainer>
    </QueryClientProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
