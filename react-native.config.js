module.exports = {
  project: {
    ios: {},
    android: {},
  },
  // assets: ['./src/assets/fonts/'], // define fonts path
  dependencies: {
    ...(process.env.NO_FLIPPER
      ? {'react-native-flipper': {platforms: {ios: null}}}
      : {}),
  },
};
