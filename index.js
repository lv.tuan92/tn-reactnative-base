/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import React from 'react';
import {Provider} from 'react-redux';
import store from './src/redux/stores';

function HeadlessCheck({isHeadless}) {
  console.log('isHeadless:   ' +  isHeadless);
  if (isHeadless) {
    // App has been launched in the background by iOS, ignore
    return null;
  }

  return <Root />;
}

const Root = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

AppRegistry.registerComponent(appName, () => HeadlessCheck);
